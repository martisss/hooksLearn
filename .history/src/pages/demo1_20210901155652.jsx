
const visibleContext = React.createContext()

const visibleReducer = (state, action) => {
  switch (action.type) {
    case "CREATE":
      return { ...state, ...action.payload}
    case "EDIT":
      return { ...state, ...action.payload}
  }
}