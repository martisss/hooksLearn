import React, { useRef, useEffect, useState, useCallback } from "react";

function Child({ event, data }) {
  console.log("child-render");
  // 第五版
  useEffect(() => {
    console.log("child-useEffect");
    event();
  }, [event]);
  return (
    <div>
      <p>child</p>
      {/* <p>props-data: {data.data && data.data.openCode}</p> */}
      <button onClick={event}>调用父级event</button>
    </div>
  );
}

const set = new Set()

function Demo2() {
  const [count, setCount] = useState(0);
  const [data, setData] = useState({});

  const handle = async () => {
    const response = await fetch(
      "https://www.mxnzp.com/api/lottery/common/latest?code=ssq"
    );
    const res = await response.json();
    console.log("handle", data);
    setData(res);
  };
  set.add(handle);

  console.log("parent-render====>", data);

  return (
    <div>
      <button
        onClick={e => {
          setCount(count + 1);
        }}
      >
        count++
      </button>
      <p>set size: {set.size}</p>
      <p>count:{count}</p>
      <p>data: {data.data && data.data.openCode}</p>
      <p>-------------------------------</p>
      <Child event={handle} />
    </div>
  );
}
export { Demo2 };


