import React, { useReducer, useContext } from 'react'

const visibleContext = React.createContext()

const visibleReducer = (state, action) => {
  switch (action.type) {
    case "CREATE":
      return { ...state, ...action.payload }
    case "EDIT":
      return { ...state, ...action.payload }
    case "RESET":
      return { ...state, ...action.payload }
    default:
      return state
  }
}

function Demo1() {
  const initVisible = {
    create: false,
    edit: false
  }
  const [state, dispatch] = useReducer(visibleReducer, initVisible)

  return (
    <visibleContext.Provider value = {{ visibles: state, dispatch}}>
      <Demo1Child />
    </visibleContext.Provider>
  )
}

function Demo1Child () {
  return (
    <div>
      Demo1Child
      <Detail />
    </div>
  )
}

function Detail() {
  const { visibles, dispatch} = useContext(visibleContext)
  console.log("contextValue", visibles)
  return (
    <div>
      <p>create: {`${visibles.create}`}</p>
      <p>edit: {`${visibles.edit}`}</p>
      <button onClick={() => dispatch({type: "CREATE", payload: { create: true}})} >create</button>
      <button onClick={() => vis dispatch({type: "EDIT", payload: { edit: true}})} >edit</button>
      <button onClick={() => dispatch({type: "RESET", payload: { create: false, edit: false}})} >reset</button>
    </div>
  )
}

export { Demo1 }