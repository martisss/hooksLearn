import React, { useRef, useEffect, useState, useCallback } from "react";


const set = new Set()
/* 
第一版
点击count++, 触发页面刷新，set size + 2 (生成新的handle)

*/
function Child({ event, data }) {
  console.log("child-render");
  useEffect(() => {
    console.log("child-useEffect");
    event();
  }, [event]);
  return (
    <div>
      <p>child</p>
      {/* <p>props-data: {data.data && data.data.openCode}</p> */}
      <button onClick={event}>调用父级event</button>
    </div>
  );
}

/* TODO */
function Demo2() {
  const [count, setCount] = useState(0);
  const [data, setData] = useState({});
  const handle = async () => {
    console.log('父级handle调用啦！')

  };

  console.log("parent-render====>", data);

  return (
    <div>
      <button
        onClick={e => {
          setCount(count + 1);
        }}
      >
        count++
      </button>
      <p>set size: {set.size}</p>
      <p>count:{count}</p>
      <p>data: {data.data && data.data.openCode}</p>
      <p>-------------------------------</p>
      <Child event={handle} />
    </div>
  );
}
export { Demo2 };


