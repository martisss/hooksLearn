import React, { useRef, useEffect, useState, useCallback } from “react”;

function Child({ event, data }) {
  console.log("child-render");
  // 第五版
  useEffect(() => {
    console.log(“child-useEffect”);
    event();
  }, [event]);
  return (
    <div>
      <p>child</p>
      {/* <p>props-data: {data.data && data.data.openCode}</p> */}
      <button onClick={event}>调用父级event</button>
    </div>
  );
}

作者：郭盖_
链接：https://juejin.cn/post/6844904018817318926
来源：掘金
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。