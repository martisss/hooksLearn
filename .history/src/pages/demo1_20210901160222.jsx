
const visibleContext = React.createContext()

const visibleReducer = (state, action) => {
  switch (action.type) {
    case "CREATE":
      return { ...state, ...action.payload}
    case "EDIT":
      return { ...state, ...action.payload}
    default:
      return state
  }
}

function Demo1() {
  const initVisible = {
    create: false,
    edit: false
  }
  const [state, dispatch] = useReducer(visibleReducer, initVisible)

  return (
    <visibleContext.Provider value = {{ visibles: state,}}>
      <Demo1Child />
    </visibleContext.Provider>
  )
}

function Demo1Child () {
  return (
    <div>
      Demo1Child
      <Detail />
    </div>
  )
}

function Detial() {
  const {vi}
}