import { Demo1, Demo2 } from './pages/demo1';
import './App.css';

function App() {
  return (
      <div class = "app">
        {/* <Demo1 /> */}
        <Demo2 />
      </div>
  );
}

export default App;
